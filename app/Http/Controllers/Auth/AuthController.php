<?php

namespace App\Http\Controllers\Auth;

use JWTAuth;
use App\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Http\Exception\HttpResponseException;

class AuthController extends Controller
{
    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|max:100',
                'password' => 'required',
            ]);
        } catch (HttpResponseException $e) {
            return response()->json([
                'error' => [
                    'message' => 'invalid_auth',
                    'status_code' => IlluminateResponse::HTTP_BAD_REQUEST,
                ],
            ], 200);
        }

        $credentials = $this->getCredentials($request);
        $u = \App\User::where('email', $request->email)->get();
        if(count($u) == 1 && isset($u[0]->expiration)) {
            $timestamp = strtotime(substr($u[0]->expiration, 0, 10));

            $dia = date('d', $timestamp);
            $mes = date('m', $timestamp);
            $ano = date('Y', $timestamp);

            $diaActual = date('d');
            $mesActual = date('m');
            $anoActual = date('Y');

            if(
                intval($ano) < intval($anoActual) 
                || (
                    intval($ano) === intval($anoActual) && (
                        intval($mes) > intval($mesActual) || (
                            intval($mes) === intval($mesActual) && intval($diaActual) >= intval($dia)
                        )
                    )
                )
            )
            {
                return response()->json([
                        'error' => [
                            'message' => 'Esta cuenta ha expirado',
                        ]
                    ], 200);
            }
        }
        else{
            if(count($u) == 0)
                return response()->json([
                        'error' => [
                            'message' => 'Usuario no encontrado',
                        ]
                    ], 200);
        }
        try {
            // Attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                        'error' => [
                            'message' => 'El usuario o la contraseña son incorrectos',
                        ]
                    ], 200);
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return response()->json([
                        'error' => [
                    'message' => 'could_not_create_token',
                 ]
            ], 200);
        }
        $now = new \DateTime('now');
        $h = $now->format('H');
        $d = $now->format('N');
        $horarioNoPermitido = false;
        if(intval($d) == 6 || intval($d) == 7) // no se permite acceder los dias sabados ni domingo
            $horarioNoPermitido = true;

        if(intval($h) < 7 || intval($h) >= 16) // no se permite acceder antes de las 7 am o despues de las 4 pm
            $horarioNoPermitido = true;

        if($horarioNoPermitido) {
            if(intval($u[0]->id) != 35) 
            {
                if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR']))
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

                $ip = isset($ip)? $ip : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
                Log::create([
                    'ip' => $ip,
                    'user' => $u[0]->email,
                    'nota' => 'Intentó iniciar sesión fuera del orario permitido',
                    'dispositivo' => $_SERVER['HTTP_USER_AGENT'],
                    'created_at' => \Carbon\Carbon::now()->format('Y-d-m H:i:s')
                ]);
            }
            return response()->json([
                'error' => [
                    'message' => 'El horario de consulta es los dias Lunes a Viernes de 7am a 4pm',
                ]
            ], 200);
        }
        // All good so return the token
        return response()->json([
            'success' => [
                'message' => 'token_generated',
                'token' => $token,
            ]
        ]);
    }
    public function mobilePostLogin(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|max:100',
                'password' => 'required',
            ]);
        } catch (HttpResponseException $e) {
            return response()->json([
                'error' => [
                    'message' => 'invalid_auth',
                    'status_code' => IlluminateResponse::HTTP_BAD_REQUEST,
                ],
            ], 200);
        }

        $credentials = $this->getCredentials($request);
        $u = \App\User::where('email', $request->email)->get();
        if(count($u) == 1 && isset($u[0]->expiration)) {
            $timestamp = strtotime(substr($u[0]->expiration, 0, 10));

            $dia = date('d', $timestamp);
            $mes = date('m', $timestamp);
            $ano = date('Y', $timestamp);

            $diaActual = date('d');
            $mesActual = date('m');
            $anoActual = date('Y');

            if(
                intval($ano) < intval($anoActual) 
                || (
                    intval($ano) === intval($anoActual) && (
                        intval($mes) > intval($mesActual) || (
                            intval($mes) === intval($mesActual) && intval($diaActual) >= intval($dia)
                        )
                    )
                )
            )
            {
                return response()->json([
                        'error' => [
                            'message' => 'Esta cuenta ha expirado',
                        ]
                    ], 200);
            }
        }
        else{
            if(count($u) == 0)
                return response()->json([
                        'error' => [
                            'message' => 'Usuario no encontrado',
                        ]
                    ], 200);
        }
        try {
            // Attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                        'error' => [
                            'message' => 'El usuario o la contraseña son incorrectos',
                        ]
                    ], 200);
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return response()->json([
                        'error' => [
                    'message' => 'could_not_create_token',
                 ]
            ], 200);
        }
        if(intval($u[0]->acceso_movil) == 0) {
            if(intval($u[0]->id) != 35) 
            {
                if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR']))
                        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

                $ip = isset($ip)? $ip : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');

                Log::create([
                    'ip' => $ip,
                    'user' => $u[0]->email,
                    'nota' => 'Intentó acceder desde un dispositivo movíl',
                    'dispositivo' => $_SERVER['HTTP_USER_AGENT'],
                    'created_at' => \Carbon\Carbon::now()->format('Y-d-m H:i:s')
                ]);
            }
            return response()->json([
                'error' => [
                    'message' => 'Este usuario no tiene permiso de acceder al sistema desde un dispositivo movíl',
                ]
            ], 200);
        }
        // All good so return the token
        return response()->json([
            'success' => [
                'message' => 'token_generated',
                'token' => $token,
            ]
        ]);
    }
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }
    public function resetPassword(Request $request)
    {
        $response = false;
        if($request->has('user')) {
            $user = \App\User::where('email', $request->only('user')['user'])->where('password', '123')->get();
            if(count($user) == 1)
                $response = true;
            else
            {
                $request['email'] = $request->only('user')['user'];
                $request['password'] = app('hash')->make('123');
                return self::postLogin($request);
            }
        }
        return ['response' => $response];
    }
    public function updateExpiration(Request $request)
    {
       $response = false;
        if($request->has('user')) {
            $user = \App\User::where('email', $request->user)->get();
            if(count($user) == 1)
            {
                $user = \App\User::find($user[0]->id);
                $user->expiration = $request->fecha;
                $user->save();
                $response = true;
            }
        }
        return ['response' => $response];
    }
    public function changePassword(Request $request)
    {
        $response = false;
        if($request->has('email')) {
            $user = \App\User::where('email', $request->email)->where('password', '123')->get();
            if(count($user) == 1)
            {
                $user = \App\User::find($user[0]->id);
                $user->password = app('hash')->make($request->password);
                $user->save();
                $response = true;
            }
        }
        return ['response' => $response];
    }
    public function newPassword(Request $request)
    {
        $response = false;
        if($request->has('email')) {
            $user = \App\User::where('email', $request->email)->get();
            if(count($user) == 1)
            {
                $user = \App\User::find($user[0]->id);
                $user->password = 123;
                $user->save();
                $response = true;
            }
        }
        return ['response' => $response];
    }
    public function toggleMobileAccess(Request $request)
    {
        $response = false;
        if($request->has('email')) {
            $user = \App\User::where('email', $request->email)->get();
            if(count($user) == 1)
            {
                $user = \App\User::find($user[0]->id);
                $user->acceso_movil = intval($user->acceso_movil) == 0? 1: 0;
                $user->save();
                $response = true;
            }
        }
        return ['response' => $response];
    }
    public function getInvalidate()
    {
        $token = JWTAuth::parseToken();
        $token->invalidate();
        return ['success' => 'token_invalidated'];
    }
    public function create(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|unique:usuarios_sre|max:100'
            ]);
        } catch (HttpResponseException $e) {
            return response()->json([
                'error' => [
                    'message' => 'invalid_auth',
                    'status_code' => IlluminateResponse::HTTP_BAD_REQUEST,
                ],
            ], 200);
        }
        $user = new \App\User;
        // $user->created_at = \Carbon\Carbon::now()->format('Y-d-m H:i:s');
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = '123';
        $user->expiration = $request->has('fecha')? $request->fecha: null;
        $user->acceso_movil = 0;
        $user->save();
        return $user;
    }
}