<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Nacimiento extends Model {
    protected $table = 'dbo.nacimientos';
    public $timestamps = false;
    protected $fillable = [
        'CVE_OFICIALIA','ANO','TRAMITE','SERVICIO','ACTA','FOJA','LIBRO','TOMO','FECHA_REGISTRO','NOMBRE','PRIMER_APELLIDO','SEGUNDO_APELLIDO',
        'SEXO','FECHA_NACIMIENTO','CVE_ESTADO','CVE_MUNICIPIO','CVE_LOCALIDAD','LOCALIDAD','REGISTRO_VIVO','CRIP','CURP','CERTIFICADO','CVE_COMPARECE',
        'IMAGEN_VALIDA','LOGIN','CVE_PAIS','OBSERVACION','OBSERVACION2','CVE_MODULO','PATERNIDAD','SUBROGADO'
    ];

    protected $hidden = [
        'FOJA','LIBRO','TOMO','CVE_ESTADO','CVE_MUNICIPIO','CVE_LOCALIDAD','REGISTRO_VIVO','CRIP','CURP','CERTIFICADO','CVE_COMPARECE',
        'IMAGEN_VALIDA','LOGIN','CVE_PAIS','OBSERVACION','OBSERVACION2','CVE_MODULO','PATERNIDAD','SUBROGADO'


        
    ];
    public function nacionalidad()
    {
        return $this->belongsTo('App\Nacionalidad', 'CVE_PAIS', 'CVE_NACIONALIDAD');
    }
    public function scopeNoLock($query)
    {
        return $query->from(\DB::raw(self::getTable() . ' with (nolock)'));
    }
}