<?php

namespace App\Http\Controllers;

use PDF;
use JWTAuth;
use App\Log;
use App\TipoDetalle;
use App\Detalle;
use App\Nacimiento;
use App\Defuncion;
use App\Nacionalidad;
use App\Oficialia;
use App\Http\Controllers\BaseApiController;
use App\Model\Repository\PostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request as RequestSupport;
use App\Model\ActivityStreams\OrderedCollection;
use App\Http\Business\ActivityBusiness;
use App\Model\Repository\FollowingRepository;

class ConsultaController extends BaseApiController {
    protected $repository = 'App\Model\Repository\PostRepository';
    public function getRegistros(Request $request) {
    	$tipo_consulta = ['NAC', 'DEF', 'DETALLES_NAC', 'DETALLES_DEF', 'DETALLES_MAT', 'DETALLES_DIV'];
    	$now = new \DateTime('now');
        $h = $now->format('H');
        $d = $now->format('N');

        try {
	        if(!$user = JWTAuth::parseToken()->toUser()) 
	            return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
	        return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
	        return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
	        return response()->json(null, 422);
	    }

	    $horarioNoPermitido = false;
        if(intval($d) == 6 || intval($d) == 7) // no se permite acceder los dias sabados ni domingo
        	$horarioNoPermitido = true;

        if(intval($h) < 7 || intval($h) >= 16) // no se permite acceder antes de las 7 am o despues de las 4 pm
			$horarioNoPermitido = true;

    	if($horarioNoPermitido) {
    		if(intval($user->id) != 35) 
    		{
			    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			        if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR']))
			            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

				$ip = isset($ip)? $ip : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');

		        Log::create([
	                'ip' => $ip,
	                'user' => $user->email,
	                'nombre' => $request->has('nombre')? $request->nombre: null,
	                'primer_apellido' => $request->has('primer_apellido')? $request->primer_apellido: null,
	                'segundo_apellido' => $request->has('segundo_apellido')? $request->segundo_apellido: null,
	                'sexo' => $request->has('sexo')? (intval($request->sexo) == 1? 'M': 'F'): null,
	                'tipoacta' => $tipo_consulta[intval($request->busqueda) - 1],
	                'nota' => 'Consultó fuera del horario permitido, desde un dispositivo móvil',
	                'dispositivo' => $_SERVER['HTTP_USER_AGENT'],
	                'fecha' => $request->has('fecha')? $request->fecha: null,
	                'created_at' => \Carbon\Carbon::now()->format('Y-d-m H:i:s')
	            ]);
		    }
        	//return response()->json(['error' => 'El horario de consultas es de lunes a viernes de 7am a 4pm (UTC-7)'], 422);
    	}

    	if(!$request->has('busqueda'))
    		return [];
    	
    	if(intval($request->busqueda) == 1 || intval($request->busqueda) == 2) { // NACIMIENTOS Y DEFUNCIONES
	    	$this->validate($request, [
	            'sexo' => 'required',
	            'fecha' => 'required',
	            'nombre' => 'required'
	        ]);

	        $request->nombre = trim($request->nombre, " ");
	        $request->primer_apellido = trim($request->primer_apellido, " ");
	        $request->segundo_apellido = trim($request->segundo_apellido, " ");

		    if(intval($user->id) != 35) {
			    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			        if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR']))
			            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

			    $ip = isset($ip)? $ip : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');

		        Log::create([
	                'ip' => $ip,
	                'user' => $user->email,
	                'nombre' => $request->nombre,
	                'primer_apellido' => $request->primer_apellido,
	                'segundo_apellido' => $request->segundo_apellido,
	                'sexo' => intval($request->sexo) == 1? 'M': 'F',
	                'tipoacta' => $tipo_consulta[intval($request->busqueda) - 1],
	                'dispositivo' => $_SERVER['HTTP_USER_AGENT'],
	                'fecha' => $request->fecha,
	                'created_at' => \Carbon\Carbon::now()->format('Y-d-m H:i:s')
	            ]);
	            //	\Carbon\Carbon::parse(\Carbon\Carbon::now('America/Mazatlan')->format('Y-d-m H:i:s'))->addHour()
		    }
	    }
	    else if(intval($request->busqueda) == 3 || intval($request->busqueda) == 4 || intval($request->busqueda) == 5 || intval($request->busqueda) == 6) // DETALLES DE UN NACIMIENTO, DEFUNCION, MATRIMONIO O DIVORCIO
	    	$this->validate($request, [
	            'cve_oficialia' => 'required',
	            'ano' => 'required',
	            'tramite' => 'required',
	            'servicio' => 'required'
	        ]);

        if(intval($request->busqueda) == 1) // NACIMIENTOS
        {
        	//return $request->segundo_apellido;
        	$salida = Nacimiento::noLock()->where('nombre', $request->nombre)
        			->where(function ($q) use ($request) {
        				if(strlen($request->primer_apellido) > 0)
					    	$q->where('primer_apellido', $request->primer_apellido);
					    else
				          	$q->where('primer_apellido', 'LIKE', '-%');
        				if(strlen($request->segundo_apellido) > 0)
					    	$q->where('segundo_apellido', $request->segundo_apellido);
					    else
				          	$q->where('segundo_apellido', 'LIKE', '-%');
				        $q->whereDay('fecha_nacimiento', '=', substr($request->fecha, 0, 2));
					    $q->whereMonth('fecha_nacimiento', '=', substr($request->fecha, 3, 2));
					    $q->whereYear('fecha_nacimiento', '=', substr($request->fecha, 6, 4));
					    $q->where('STATUS', '<', 30);
				    })
				    ->join('servicios', function($join) {
	                    $join->on('nacimientos.cve_oficialia', '=',  'servicios.cve_oficialia');
	                    $join->on('nacimientos.ano', '=',  'servicios.ano');
	                    $join->on('nacimientos.tramite', '=',  'servicios.tramite');
	                    $join->on('nacimientos.servicio', '=',  'servicios.servicio');
	                })
					->get();
			for ($i=0; $i < sizeof($salida); $i++) { 
				unset($salida[$i]->CANT);
				unset($salida[$i]->CVE_SERVICIO);
				unset($salida[$i]->TIPO_SERVICIO);
				unset($salida[$i]->DESCRIPCION);
				unset($salida[$i]->IMPORTE);
				unset($salida[$i]->ADMINISTRATIVO);
			}
			
			return $salida;
        }
		else if(intval($request->busqueda) == 2) // DEFUNCIONES
		{
			$salida = Defuncion::noLock()->where('nombre', $request->nombre)
    			->where(function ($q) use ($request) {
    				if(strlen($request->primer_apellido) > 0)
				    	$q->where('primer_apellido', $request->primer_apellido);
				    else
			          	$q->where('primer_apellido', 'LIKE', '-%');
    				if(strlen($request->segundo_apellido) > 0)
				    	$q->where('segundo_apellido', $request->segundo_apellido);
				    else
			          	$q->where('segundo_apellido', 'LIKE', '-%');
			        $q->whereDay('fecha_registro', '=', substr($request->fecha, 0, 2));
				    $q->whereMonth('fecha_registro', '=', substr($request->fecha, 3, 2));
				    $q->whereYear('fecha_registro', '=', substr($request->fecha, 6, 4));
				    $q->where('STATUS', '<', 30);
			    })
			    ->join('servicios', function($join) {
                    $join->on('defunciones.cve_oficialia', '=',  'servicios.cve_oficialia');
                    $join->on('defunciones.ano', '=',  'servicios.ano');
                    $join->on('defunciones.tramite', '=',  'servicios.tramite');
                    $join->on('defunciones.servicio', '=',  'servicios.servicio');
				})
				->get();
			
			for ($i=0; $i < sizeof($salida); $i++) { 

				unset($salida[$i]->CANT);
				unset($salida[$i]->CVE_SERVICIO);
				unset($salida[$i]->TIPO_SERVICIO);
				unset($salida[$i]->DESCRIPCION);
				unset($salida[$i]->IMPORTE);
				unset($salida[$i]->ADMINISTRATIVO);

				if($salida[$i]->CVE_OFICIALIA) {
					if(array_search($salida[$i]->CVE_OFICIALIA, ['06001', '06007', '06012', '06016'])) {
						$salida[$i]->LOCALIDAD = 'CULIACAN';
					}
					else {
						$loc = Oficialia::noLock()->find($salida[$i]->CVE_OFICIALIA);
						if($loc)
							$salida[$i]->LOCALIDAD = $loc->OFICIALIA;
					}
				}
			}
			if($salida)
				return $salida;
			else
				return [];
		}
		else if(intval($request->busqueda) == 3) // DETALLES DE UN ACTA DE NACIMIENTO
		{
			$d = Detalle::noLock()->with('tipo_detalle', 'nacionalidad')
				->where('cve_oficialia', $request->cve_oficialia)
    			->where('ano', $request->ano)
    			->where('tramite', $request->tramite)
    			->where('servicio', $request->servicio)
			    ->get();
			$n = Nacimiento::noLock()->with('nacionalidad')->where('nacimientos.cve_oficialia', $request->cve_oficialia)
    			->where('nacimientos.ano', $request->ano)
    			->where('nacimientos.tramite', $request->tramite)
    			->where('nacimientos.servicio', $request->servicio)
    			->join('servicios', function($join) {
                    $join->on('nacimientos.cve_oficialia', '=',  'servicios.cve_oficialia');
                    $join->on('nacimientos.ano', '=',  'servicios.ano');
                    $join->on('nacimientos.tramite', '=',  'servicios.tramite');
                    $join->on('nacimientos.servicio', '=',  'servicios.servicio');
                })
    			->limit(1)
			    ->get();
			if(count($n) > 0) {
				for ($i=0; $i < sizeof($n); $i++) { 
					unset($n[$i]->ANO);
					unset($n[$i]->TRAMITE);
					unset($n[$i]->CANT);
					unset($n[$i]->CVE_SERVICIO);
					unset($n[$i]->TIPO_SERVICIO);
					unset($n[$i]->DESCRIPCION);
					unset($n[$i]->IMPORTE);
					unset($n[$i]->ADMINISTRATIVO);
					unset($n[$i]->EXTNVA);
					unset($n[$i]->NUEVA);
					unset($n[$i]->LOCALIDAD2);
					unset($n[$i]->SERVICIO);
					unset($n[$i]->CVE_MUNICIPIO);
					unset($n[$i]->CVE_LOCALIDAD);
					unset($n[$i]->CVE_ESTADO);
				}

				$salida = $n[0];
				
				$order = array(7, 8, 20, 21, 22, 23, 10, 11);
				$d = $d->toArray();
				usort($d, function ($a, $b) use ($order) {
					$pos_a = array_search($a['tipo_detalle']['CVE_DETALLE'], $order);
				    $pos_b = array_search($b['tipo_detalle']['CVE_DETALLE'], $order);
				    return $pos_a - $pos_b;
				});

				$salida->DETALLES = $d;
				return $salida;
			}
			else 
				return $n;
		}
		else if(intval($request->busqueda) == 4) {// DETALLES DE UN ACTA DE DEFUNCION
			$d = Detalle::noLock()->with('tipo_detalle', 'nacionalidad')
				->where('cve_oficialia', $request->cve_oficialia)
    			->where('ano', $request->ano)
    			->where('tramite', $request->tramite)
    			->where('servicio', $request->servicio)
			    ->get();
			$n = Defuncion::noLock()->with('nacionalidad')->where('defunciones.cve_oficialia', $request->cve_oficialia)
    			->where('defunciones.ano', $request->ano)
    			->where('defunciones.tramite', $request->tramite)
    			->where('defunciones.servicio', $request->servicio)
    			->join('cat_localidades', function($join) {
	                    $join->on('defunciones.cve_localidad_domicilio', '=',  'cat_localidades.cve_localidad');
	                    $join->on('defunciones.cve_estado_domicilio', '=',  'cat_localidades.cve_estado');
	                    $join->on('defunciones.cve_municipio_domicilio', '=',  'cat_localidades.cve_municipio');
	            })
    			->join('servicios', function($join) {
                    $join->on('defunciones.cve_oficialia', '=',  'servicios.cve_oficialia');
                    $join->on('defunciones.ano', '=',  'servicios.ano');
                    $join->on('defunciones.tramite', '=',  'servicios.tramite');
                    $join->on('defunciones.servicio', '=',  'servicios.servicio');
                })
    			->limit(1)
			    ->get();
			if(count($n) > 0) {
				for ($i=0; $i < sizeof($n); $i++) { 
					unset($n[$i]->ANO);
					unset($n[$i]->TRAMITE);
					unset($n[$i]->CANT);
					unset($n[$i]->CVE_SERVICIO);
					unset($n[$i]->TIPO_SERVICIO);
					unset($n[$i]->DESCRIPCION);
					unset($n[$i]->IMPORTE);
					unset($n[$i]->ADMINISTRATIVO);
					unset($n[$i]->EXTNVA);
					unset($n[$i]->NUEVA);
					unset($n[$i]->LOCALIDAD2);
					unset($n[$i]->SERVICIO);
					unset($n[$i]->CVE_MUNICIPIO);
					unset($n[$i]->CVE_LOCALIDAD);
					unset($n[$i]->CVE_ESTADO);

					if($n[$i]->CVE_OFICIALIA) {
						if(array_search($n[$i]->CVE_OFICIALIA, ['06001', '06007', '06012', '06016'])) {
							$n[$i]->LOCALIDAD = 'CULIACAN';
						}
						else {
							$loc = Oficialia::noLock()->find($n[$i]->CVE_OFICIALIA);
							if($loc)
								$n[$i]->LOCALIDAD = $loc->OFICIALIA;
						}
					}
				}

				$order = array(27, 28, 29, 9, 10, 11);
				$d = $d->toArray();
				usort($d, function ($a, $b) use ($order) {
					$pos_a = array_search($a['tipo_detalle']['CVE_DETALLE'], $order);
				    $pos_b = array_search($b['tipo_detalle']['CVE_DETALLE'], $order);
				    return $pos_a - $pos_b;
				});
				
				$salida = $n[0];
				$salida->DETALLES = $d;
				return $salida;
			}
			else 
				return $n;
		}
		else if(intval($request->busqueda) == 5) { // DETALLES DE UN ACTA DE MATRIMONIOS
			$d = Detalle::with('tipo_detalle', 'nacionalidad')
				->where('cve_oficialia', $request->cve_oficialia)
    			->where('ano', $request->ano)
    			->where('tramite', $request->tramite)
    			->where('servicio', $request->servicio)
			    ->get();
			$n = app('db')->select("select NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, FECHA_NACIMIENTO, FECHA_REGISTRO,
				(
					select localidad 
					from cat_localidades l
					where l.cve_localidad = d.cve_localidad and 
						l.cve_estado = d.cve_estado and 
						l.cve_municipio = d.cve_municipio
				) as LOCALIDAD, 
				ACTA, 
				d.CVE_OFICIALIA, 
				d.ANO, 
				d.TRAMITE, 
				d.SERVICIO,
				d.CVE_NACIONALIDAD,
				STATUS,
				d.SEXO
				from dbo.matrimonios_detalle d 
					inner join matrimonios m on d.cve_oficialia = m.cve_oficialia and d.ano = m.ano and d.tramite = m.tramite and d.servicio = m.servicio
					inner join dbo.servicios s on d.cve_oficialia = s.cve_oficialia and d.ano = s.ano and d.tramite = s.tramite and d.servicio = s.servicio
				where
					d.cve_oficialia = ".$request->cve_oficialia." and d.ano = ".$request->ano." and d.tramite = ".$request->tramite." and d.servicio = ".$request->servicio);
			for ($i=0; $i < count($n); $i++) { 
				$nacionalidad = app('db')->select("select NACIONALIDAD from cat_nacionalidades where CVE_NACIONALIDAD = ".$n[$i]->CVE_NACIONALIDAD);
				$n[$i]->nacionalidad = ['CVE_NACIONALIDAD' => $n[$i]->CVE_NACIONALIDAD, 'NACIONALIDAD' => $nacionalidad[0]->NACIONALIDAD];
				//return ['CVE_NACIONALIDAD' => $n[$i]->CVE_NACIONALIDAD, 'NACIONALIDAD' => $nacionalidad[0]->NACIONALIDAD];
			}

			if(count($n) > 0) {
				$salida = $n[0];
				$salida->DETALLES = $d;
				if(count($n) === 2)
					$salida->conyuge2 = $n[1];
				return (Array) $salida;
			}
			else 
				return $n;
		}
		else if(intval($request->busqueda) == 6) { // DETALLES DE UN ACTA DE DIVORCIO
			$d = Detalle::with('tipo_detalle', 'nacionalidad')
					->where('cve_oficialia', $request->cve_oficialia)
        			->where('ano', $request->ano)
        			->where('tramite', $request->tramite)
        			->where('servicio', $request->servicio)
				    ->get();
			$n = app('db')->select("select NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, FECHA_NACIMIENTO, FECHA_REGISTRO,
				(
					select localidad 
					from cat_localidades l
					where l.cve_localidad = d.cve_localidad and 
						l.cve_estado = d.cve_estado and 
						l.cve_municipio = d.cve_municipio
				) as LOCALIDAD, 
				ACTA, 
				d.CVE_OFICIALIA, 
				d.ANO, 
				d.TRAMITE, 
				d.SERVICIO,
				d.CVE_NACIONALIDAD,
				STATUS
				from dbo.divorcios_detalle d 
					inner join divorcios m on d.cve_oficialia = m.cve_oficialia and d.ano = m.ano and d.tramite = m.tramite and d.servicio = m.servicio
					inner join dbo.servicios s on d.cve_oficialia = s.cve_oficialia and d.ano = s.ano and d.tramite = s.tramite and d.servicio = s.servicio
				where
					d.cve_oficialia = ".$request->cve_oficialia." and d.ano = ".$request->ano." and d.tramite = ".$request->tramite." and d.servicio = ".$request->servicio);
			for ($i=0; $i < count($n); $i++) { 
				$nacionalidad = app('db')->select("select NACIONALIDAD from cat_nacionalidades where CVE_NACIONALIDAD = ".$n[$i]->CVE_NACIONALIDAD);
				$n[$i]->nacionalidad = ['CVE_NACIONALIDAD' => $n[$i]->CVE_NACIONALIDAD, 'NACIONALIDAD' => $nacionalidad[0]->NACIONALIDAD];
				//return ['CVE_NACIONALIDAD' => $n[$i]->CVE_NACIONALIDAD, 'NACIONALIDAD' => $nacionalidad[0]->NACIONALIDAD];
			}

			if(count($n) > 0) {
				$salida = $n[0];
				$salida->DETALLES = $d;
				if(count($n) === 2)
					$salida->conyuge2 = $n[1];
				return (Array) $salida;
			}
			else 
				return $n;
		}
    }
    public function consultaMovil(Request $request) {
    	$tipo_consulta = ['NAC', 'DEF', 'MAT', 'DIV'];
    	$now = new \DateTime('now');
        $h = $now->format('H');
        $d = $now->format('N');

        try {
	        if(!$user = JWTAuth::parseToken()->toUser()) 
	            return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
	        return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
	        return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
	        return response()->json(null, 422);
	    }
	    $horarioNoPermitido = false;
        if(intval($d) == 6 || intval($d) == 7) // no se permite acceder los dias sabados ni domingo
        	$horarioNoPermitido = true;

        if(intval($h) < 7 || intval($h) >= 16) // no se permite acceder antes de las 7 am o despues de las 4 pm
			$horarioNoPermitido = true;

    	if($horarioNoPermitido) {
    		if(intval($user->id) != 35) 
    		{
			    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			        if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR']))
			            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

			    $ip = isset($ip)? $ip : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');

		        Log::create([
	                'ip' => $ip,
	                'user' => $user->email,
	                'nombre' => $request->nombre,
	                'primer_apellido' => $request->primer_apellido,
	                'segundo_apellido' => $request->segundo_apellido,
	                'sexo' => $request->has('sexo') ? (intval($request->sexo) == 1? 'M': 'F'): null,
	                'tipoacta' => $tipo_consulta[intval($request->busqueda) - 1],
	                'nota' => 'Consultó fuera del horario permitido, desde un dispositivo móvil',
	                'dispositivo' => $_SERVER['HTTP_USER_AGENT'],
	                'fecha' => $request->fecha,
	                'created_at' => \Carbon\Carbon::now()->format('Y-d-m H:i:s')
	            ]);
		    }
        	//return response()->json(['error' => 'El horario de consultas es de lunes a viernes de 7am a 4pm (UTC-7)'], 422);
    	}

    	if(!$request->has('busqueda'))
    		return [];

    	if(intval($request->busqueda) == 1 || intval($request->busqueda) == 2 || intval($request->busqueda) == 4 || intval($request->busqueda) == 5) { // NACIMIENTOS, DEFUNCIONES, MATRIMONIOS, DIVORCIOS

	        $request->nombre = trim($request->nombre, " ");
	        $request->primer_apellido = trim($request->primer_apellido, " ");
	        $request->segundo_apellido = trim($request->segundo_apellido, " ");

		    if(intval($user->id) != 35) {
			    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			        if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR']))
			            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

			    $ip = isset($ip)? $ip : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
		        Log::create([
	                'ip' => $ip,
	                'user' => $user->email,
	                'nombre' => $request->nombre,
	                'primer_apellido' => $request->primer_apellido,
	                'segundo_apellido' => $request->segundo_apellido,
	                'tipoacta' => $tipo_consulta[intval($request->busqueda) - 1],
	                'dispositivo' => $_SERVER['HTTP_USER_AGENT'],
	                'fecha' => $request->has('fecha')? $request->fecha: null,
	                'created_at' => \Carbon\Carbon::now()->format('Y-d-m H:i:s')
	            ]);
	            //	\Carbon\Carbon::parse(\Carbon\Carbon::now('America/Mazatlan')->format('Y-d-m H:i:s'))->addHour()
		    }
	    }
	    if(intval($request->busqueda) == 1) // NACIMIENTOS
	    	return Nacimiento::where('nombre', 'LIKE', $request->nombre.'%')
    			->where(function ($q) use ($request) {
				    $q->where('primer_apellido', 'LIKE', $request->primer_apellido.'%');
				    $q->where('segundo_apellido', 'LIKE', $request->segundo_apellido.'%');
				    //$q->where('STATUS', '<', 30);
			        if($request->has('fecha')) {
			        	$q->whereDay('fecha_nacimiento', '=', substr($request->fecha, 0, 2));
					    $q->whereMonth('fecha_nacimiento', '=', substr($request->fecha, 3, 2));
					    $q->whereYear('fecha_nacimiento', '=', substr($request->fecha, 6, 4));
			        };
			    })
			    ->join('servicios', function($join) {
                    $join->on('nacimientos.cve_oficialia', '=',  'servicios.cve_oficialia');
                    $join->on('nacimientos.ano', '=',  'servicios.ano');
                    $join->on('nacimientos.tramite', '=',  'servicios.tramite');
                    $join->on('nacimientos.servicio', '=',  'servicios.servicio');
                })
			    ->get();
		else if(intval($request->busqueda) == 2) // DEFUNCIONES
			return Defuncion::where('nombre', 'LIKE', $request->nombre.'%')
        			->where(function ($q) use ($request) {
					    $q->where('primer_apellido', 'LIKE', $request->primer_apellido.'%');
					    $q->where('segundo_apellido', 'LIKE', $request->segundo_apellido.'%');
					   	//$q->where('STATUS', '<', 30);
				        if($request->has('fecha')) {
				        	$q->whereDay('fecha_registro', '=', substr($request->fecha, 0, 2));
						    $q->whereMonth('fecha_registro', '=', substr($request->fecha, 3, 2));
						    $q->whereYear('fecha_registro', '=', substr($request->fecha, 6, 4));
				        };
				    })
				    ->join('servicios', function($join) {
	                    $join->on('defunciones.cve_oficialia', '=',  'servicios.cve_oficialia');
	                    $join->on('defunciones.ano', '=',  'servicios.ano');
	                    $join->on('defunciones.tramite', '=',  'servicios.tramite');
	                    $join->on('defunciones.servicio', '=',  'servicios.servicio');
	                })
	                ->join('cat_localidades', function($join) {
	                    $join->on('defunciones.cve_localidad_domicilio', '=',  'cat_localidades.cve_localidad');
	                    $join->on('defunciones.cve_estado_domicilio', '=',  'cat_localidades.cve_estado');
	                    $join->on('defunciones.cve_municipio_domicilio', '=',  'cat_localidades.cve_municipio');
	                })
				    ->get();
		else if(intval($request->busqueda) == 3) // MATRIMONIOS
		{
			return app('db')->select("select NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, FECHA_NACIMIENTO, FECHA_REGISTRO,
				(
					select localidad 
					from cat_localidades l
					where l.cve_localidad = d.cve_localidad and 
						l.cve_estado = d.cve_estado and 
						l.cve_municipio = d.cve_municipio
				) as LOCALIDAD, 
				ACTA, 
				d.CVE_OFICIALIA, 
				d.ANO, 
				d.TRAMITE, 
				d.SERVICIO,
				STATUS,
				d.SEXO
				from dbo.matrimonios_detalle d 
					inner join matrimonios m on d.cve_oficialia = m.cve_oficialia and d.ano = m.ano and d.tramite = m.tramite and d.servicio = m.servicio
					inner join dbo.servicios s on d.cve_oficialia = s.cve_oficialia and d.ano = s.ano and d.tramite = s.tramite and d.servicio = s.servicio
				where
					".(
						$request->has('fecha')? 
						"day(m.fecha_registro) = ".substr($request->fecha, 0, 2)." and month(m.fecha_registro) = ".substr($request->fecha, 3, 2)." and year(m.fecha_registro) = ".substr($request->fecha, 6, 4)." and ": ""
					)."
					nombre like '".$request->nombre."%' and
					primer_apellido like '".$request->primer_apellido."%' and
					segundo_apellido like '".$request->segundo_apellido."%'
			");// and status < 30 
		}
		else if(intval($request->busqueda) == 4) // MATRIMONIOS
		{
			return app('db')->select("select NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, FECHA_NACIMIENTO, FECHA_REGISTRO,
				(
					select localidad 
					from cat_localidades l
					where l.cve_localidad = d.cve_localidad and 
						l.cve_estado = d.cve_estado and 
						l.cve_municipio = d.cve_municipio
				) as LOCALIDAD, 
				ACTA, 
				d.CVE_OFICIALIA, 
				d.ANO, 
				d.TRAMITE, 
				d.SERVICIO,
				STATUS
				from dbo.divorcios_detalle d 
					inner join divorcios m on d.cve_oficialia = m.cve_oficialia and d.ano = m.ano and d.tramite = m.tramite and d.servicio = m.servicio
					inner join dbo.servicios s on d.cve_oficialia = s.cve_oficialia and d.ano = s.ano and d.tramite = s.tramite and d.servicio = s.servicio
				where
					".(
						$request->has('fecha')? 
						"day(fecha_registro) = ".substr($request->fecha, 0, 2)." and month(fecha_registro) = ".substr($request->fecha, 3, 2)." and year(fecha_registro) = ".substr($request->fecha, 6, 4)." and ": ""
					)."
					nombre like '".$request->nombre."%' and
					primer_apellido like '".$request->primer_apellido."%' and
					segundo_apellido like '".$request->segundo_apellido."%'
			"); // and status < 30
		}
	}

    function generate_pdf(Request $request) {
    	if (!file_exists('comprobante/'))
            mkdir('comprobante/', 0777, true);

        try {
	        if(!$user = JWTAuth::parseToken()->toUser()) 
	            return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
	        return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
	        return response()->json(null, 422);
	    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
	        return response()->json(null, 422);
	    }

        $now = \DateTime::createFromFormat('U.u', microtime(true));
        $day = $now->format('d');
        $mn = $now->format('m');
        $year = $now->format('Y');
		$meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Nobiembre', 'Diciembre'];
    	$month = $meses[ intval($mn) - 1 ];
        
        $archivo = $user->email.'_'.$now->format('dmYHmsu').'.pdf';
		
    	if (file_exists('comprobante/'.$archivo)) // eliminar archivo
    		unlink('comprobante/'.$archivo);

    	$detalles = $request->detalles; //self::getRegistros($request);
        $data = [];
        $data['nombre'] = $detalles['NOMBRE'];
        $data['primer_apellido'] = $detalles['PRIMER_APELLIDO'];
		$data['segundo_apellido'] = $detalles['SEGUNDO_APELLIDO'];
        $fr = \Carbon\Carbon::parse($detalles['FECHA_REGISTRO'])->format('d/m/Y');
        $fn = \Carbon\Carbon::parse($detalles['FECHA_NACIMIENTO'])->format('d/m/Y');

        $data['fecha_registro'] = intval(substr($fr, 6, 2)) === 18? '- - - - -': $fr; // testar fechas del año 1800 a 1899
        $data['fecha_nacimiento'] = intval(substr($fn, 6, 2)) === 18? '- - - - -': $fn; // testar fechas del año 1800 a 1899
        $data['acta'] = $detalles['ACTA'];
		$data['localidad'] = strlen($detalles['LOCALIDAD']) === 0? '- - - - -': $detalles['LOCALIDAD'];

        $data['municipio'] = 'Culiacán';
        $data['estado'] = 'Sinaloa';
        $data['day'] = $day;
        $data['month'] = $month;
        $data['year'] = $year;
        $data['tipo_comprobante'] = intval($request->busqueda) == 3? 'nacimiento': 'defunción';

        /* MOSTRAR SOLO LOS SIGUIENTES DETALLES
        	para nacimientos
        		el padre (ocultar edad)
        		la madre (ocultar edad)
        		abuelo paterno
        		abuela paterna
        		abuelo materno
        		abuela materna
			para defunciones
				conyuge del finado (ocultar edad)
				padre del finado (ocultar edad)
				madre del finado (ocultar edad)
				declarante
				primer testigo
				segundo testigo
        */
		// insertar detalles testados en caso de no existir en la bd
		$tiene_detalle = function($arr, $val) {
			$td = FALSE;
			for ($i=0; $i < count($arr); $i++) { 
				if(intval($arr[$i]['tipo_detalle']['CVE_DETALLE']) === intval($val))
				{
					$td = TRUE;
					break;
				}
			}
			return $td;
		};

		$detalles_imprimir = [];
		for ($i=0; $i < count($detalles['DETALLES']); $i++) { 
			$td = intval($detalles['DETALLES'][$i]['tipo_detalle']['CVE_DETALLE']);
			if($td === 7 || $td === 8 || $td === 28 || $td === 29 || $td === 27) // padre, madre y conyugue no muestran su edad en las actas
				$detalles['DETALLES'][$i]['EDAD'] = '';
			else
			{
				if(intval($detalles['DETALLES'][$i]['EDAD']) === 999 || intval($detalles['DETALLES'][$i]['EDAD']) === 99)	// testado
					$detalles['DETALLES'][$i]['EDAD'] = '- - -';
			}
			if(intval($request->busqueda) === 3 && ($td === 7 || $td === 8 || $td === 20 || $td === 21 || $td === 22 || $td === 23)) 
        		array_push($detalles_imprimir, $detalles['DETALLES'][$i]);
			else if(intval($request->busqueda) === 4 && ($td === 27 || $td === 28 || $td === 29 || $td === 9 || $td === 10 || $td === 11))
        		array_push($detalles_imprimir, $detalles['DETALLES'][$i]);
		}

		// AGREGAR DETALLES EN CASO DE QUE NO ESTEN EN LA BASE DE DATOS
		if(intval($request->busqueda) === 3) {
			if(!$tiene_detalle($detalles_imprimir, 7)) // si no tiene padre y es acta de nacimiento
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '',
					'tipo_detalle' => ['CVE_DETALLE' => 7, 'NOM_DETALLE' => 'EL PADRE'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 8)) // si no tiene madre y es acta de nacimiento
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '',
					'tipo_detalle' => ['CVE_DETALLE' => 8, 'NOM_DETALLE' => 'LA MADRE'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 20)) // si no tiene abuelo paterno y es acta de nacimiento
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 20, 'NOM_DETALLE' => 'ABUELO PATERNO'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 21)) // si no tiene abuela paterna y es acta de nacimiento
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 21, 'NOM_DETALLE' => 'ABUELA PATERNA'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 22)) // si no tiene abuelo materno y es acta de nacimiento
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 22, 'NOM_DETALLE' => 'ABUELO MATERNO'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 23)) // si no tiene abuela materna y es acta de nacimiento
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 23, 'NOM_DETALLE' => 'ABUELA MATERNA'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
		}
		else if (intval($request->busqueda) === 4) {
			if(!$tiene_detalle($detalles_imprimir, 27)) // si no tiene abuela paterna y es acta de defunción
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 27, 'NOM_DETALLE' => 'CONYUGE DEL FINADO'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 28)) // si no tiene padre y es acta de defunción
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '',
					'tipo_detalle' => ['CVE_DETALLE' => 28, 'NOM_DETALLE' => 'PADRE DEL FINADO'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 29)) // si no tiene madre y es acta de defunción
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '',
					'tipo_detalle' => ['CVE_DETALLE' => 29, 'NOM_DETALLE' => 'MADRE DEL FINADO'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);

			if(!$tiene_detalle($detalles_imprimir, 9)) // si no tiene abuela paterna y es acta de defunción
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 9, 'NOM_DETALLE' => 'DECLARANTE'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 10)) // si no tiene abuela paterna y es acta de defunción
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 10, 'NOM_DETALLE' => 'PRIMER TESTIGO'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
			if(!$tiene_detalle($detalles_imprimir, 11)) // si no tiene abuela paterna y es acta de defunción
				array_push($detalles_imprimir, [
					'NOMBRE' => '- - - - -', 
					'PRIMER_APELLIDO' => '- - - - -', 
					'SEGUNDO_APELLIDO' => '- - - - -', 
					'NACIONALIDAD' => '- - - - -', 
					'EDAD' => '- - -',
					'tipo_detalle' => ['CVE_DETALLE' => 11, 'NOM_DETALLE' => 'SEGUNDO TESTIGO'],
					'nacionalidad' => ['CVE_NACIONALIDAD' => 999, 'NACIONALIDAD' => '- - - - -']
				]);
		}

		// oder $detalles_imprimir by tipo_detalle->CVE_DETALLE
		if(intval($request->busqueda) === 3)
		{
			// nacimientos lleva detalles en orden 7, 8, 20, 21, 22, 23 (de menor a mayor)
			usort($detalles_imprimir, function ($a, $b) {
			    return $a['tipo_detalle']['CVE_DETALLE'] - $b['tipo_detalle']['CVE_DETALLE'];
			});
		}
		else
		{
			if (intval($request->busqueda) === 4)
			{
				// defunciones lleva detalles en orden 27, 28, 29, 9, 10, 11
				$order = array(27, 28, 29, 9, 10, 11);
				usort($detalles_imprimir, function ($a, $b) use ($order) {
					$pos_a = array_search($a['tipo_detalle']['CVE_DETALLE'], $order);
				    $pos_b = array_search($b['tipo_detalle']['CVE_DETALLE'], $order);
				    return $pos_a - $pos_b;
				});
			}
		}

        $data['detalles'] = $detalles_imprimir;

        $pdf = PDF::loadView('comprobanteNuevo', $data);
        $pdf->save('comprobante/'.$archivo);
        

		if(intval($user->id) != 35) 
		{
		    if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		        if (preg_match( "/^([d]{1,3}).([d]{1,3}).([d]{1,3}).([d]{1,3})$/", $_SERVER['HTTP_X_FORWARDED_FOR']))
		            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

		    $ip = isset($ip)? $ip : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');

	        Log::create([
                'ip' => $ip,
                'user' => $user->email,
                'nombre' => $detalles['NOMBRE'],
                'primer_apellido' => $detalles['PRIMER_APELLIDO'],
                'segundo_apellido' => $detalles['SEGUNDO_APELLIDO'],
                'tipoacta' => intval($request->busqueda) == 3? 'NAC': 'DEF',
                'nota' => 'Generó PDF',
                'dispositivo' => $_SERVER['HTTP_USER_AGENT'],
                'created_at' => \Carbon\Carbon::now()->format('Y-d-m H:i:s')
            ]);
	    }

	    /*
        $file = base_path('consultasrc/comprobante/'.$archivo);
        $pdfdata = file_get_contents($file);
        $base64 = base64_encode($pdfdata);
        
        return ['data' => $base64];
        */
        return ['file' => $archivo];
    }
}