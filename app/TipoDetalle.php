<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class TipoDetalle extends Model {
    protected $table = 'dbo.cat_tipo_detalle';
    protected $primaryKey = 'CVE_DETALLE';
    public $timestamps = false;
    protected $fillable = [
        'NOM_DETALLE',
        'CVE_DETALLE'
    ];

    protected $hidden = [
        
    ];

    public function detalles()
    {
        return $this->hasMany('App\Detalle', 'CVE_DETALLE');
    }
}