<!DOCTYPE html>
<html>
<head>
	<title>Comprobante</title>
	<style type="text/css">
		@page {
		    header: page-header;
		    footer: page-footer;
		}
		body {
			color: #333;
		    font-family: 'roboto', serif;
		    font-size: 16px;
		}
		#date {
			margin-top: -120px;
			margin-right: 20px;
			text-align: right;
		}
		#logo{
			margin-left: 20px;
		}
		img{
			margin-left: 30px;
		}
		p{
			text-align: center;
		}
		.content {
			margin-left: 1em;
			margin-right: 1em;
		}
		.ft {
			color: #555;
			font-size: 0.8em !important;
		}
		table {
		    border-collapse: collapse;
			font-size: 1em !important;
		    width: 100%;
		}

		td, th {
		    text-align: left;
		    padding: 0.6em;
		}

		tr:nth-child(even) {
		    background-color: #f0f0f0;
		}
	</style>
	<!--border: 1px solid #eee;-->
</head>
<body>
	<htmlpageheader name="page-header">
		<br><br>
	    <div id="logo"><img src="img/gob-logo.jpg" alt="logo" height="112px" width="90px"></div>
	    <div id="date"><img src="img/reg-logo.jpg" alt="logo" height="87px" width="110px"><br>Culiacán, Sinaloa {{$day}} de {{$month}} de {{$year}}</div>
	    <br><p>DIRECCIÓN DEL REGISTRO CIVIL</p>
	</htmlpageheader>
		<div class="content">
			<p><br><br><br><br><br><br><br><br><br></p>
			<br>Comprobante de registro de {{$tipo_comprobante}}
			<br>
      <br>Nombre: <b>{{$nombre}} {{$primer_apellido}} {{$segundo_apellido}}</b>
      <br>Fecha de registro: <b>{{$fecha_registro}}</b>
      <br>Fecha de nacimiento: <b>{{$fecha_nacimiento}}</b>
      <br>
      <br>Número de acta: <b>{{$acta}}</b>
      <br>Localidad: <b>{{$localidad}}</b>
			<br><br>
			<table>
			  <tr>
			  	<th>Persona</th>
			  	<th>Nombre</th>
			    <th>Primer apellido</th>
			    <th>Segundo apellido</th>
				<th>Nacionalidad</th>
			    <th>Edad</th>
			  </tr>

        <!-- for -->
        @forelse ($detalles as $index => $detalle)
        <tr>
          <td>
            <small><strong>{{$detalle['tipo_detalle']['NOM_DETALLE']}}</strong></small>
          </td>
          <td>
            {{$detalle['NOMBRE']}}
          </td>
          <td>
            {{$detalle['PRIMER_APELLIDO']}}
          </td>
          <td>
            {{$detalle['SEGUNDO_APELLIDO']}}
          </td>
          <td>
            {{$detalle['nacionalidad']['NACIONALIDAD']}}
          </td>
          <td>
            {{$detalle['EDAD']}}
          </td>
        </tr>
        @empty
        @endforelse
        <!-- for -->

			</table>
		</div>
</body>
</html>