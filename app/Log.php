<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Log extends Model {
    protected $table = 'log_sre';
    protected $fillable = [
    	'id',
    	'dispositivo',
    	'ip',
    	'user',
    	'nombre',
    	'primer_apellido',
    	'segundo_apellido',
    	'sexo',
    	'fecha',
    	'tipoacta',
    	'nota',
    	'created_at',
    ];
    protected $hidden = [];
    public $timestamps = false;
}