<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => '/'], function () use ($app) {

    $app->get('/', function () use ($app) {
        return view('index');
    });

    $app->get('auth/invalidate', 'App\Http\Controllers\Auth\AuthController@getInvalidate');
    $app->post('auth/login', 'App\Http\Controllers\Auth\AuthController@postLogin');
    $app->post('auth/login2', 'App\Http\Controllers\Auth\AuthController@mobilePostLogin');
    $app->post('auth/update_expiration', 'App\Http\Controllers\Auth\AuthController@updateExpiration');
    $app->post('auth/user', 'App\Http\Controllers\Auth\AuthController@create');
    $app->get('auth/password_reset', 'App\Http\Controllers\Auth\AuthController@resetPassword'); // checar si el usuario tiene 123 en el campo password de la tabla usuarios_sre
    $app->post('auth/reset_password', 'App\Http\Controllers\Auth\AuthController@changePassword'); // cambiar contraseña
    $app->post('auth/new_password', 'App\Http\Controllers\Auth\AuthController@newPassword'); // poner 123 en el campo password de la tabla usuarios_sre
    $app->post('auth/toggle_mobile_access', 'App\Http\Controllers\Auth\AuthController@toggleMobileAccess'); // poner 123 en el campo password de la tabla usuarios_sre

    
    $app->group(['middleware' => 'jwt.auth', 'prefix' => 'api/v1/'], function ($app) {
        $app->get('version', function () use ($app) {
            return [
                'success' => [
                    'app' => $app->version(),
                ],
            ];
        });

        $app->get('users', function () use ($app) {
            $users = \App\User::all();
            for ($i=0; $i < count($users); $i++) { 
                $u = \App\User::where('password', '123')->where('id', $users[$i]->id)->get();
                $users[$i]->reseteado = count($u) === 1? true: false;
            }
            return $users;
        });
       
        $app->get('user', function () use ($app) {
            return [
                'success' => [
                    'user' => JWTAuth::parseToken()->authenticate(),
                ],
            ];
        });

        $app->delete('user/{id}', function($id)
        {
            return \App\User::where('id', $id)->delete();
        });
        
        $app->post('get_registros', 'App\Http\Controllers\ConsultaController@getRegistros');
        $app->post('consulta_movil', 'App\Http\Controllers\ConsultaController@consultaMovil');
        $app->post('genera_comprobante', 'App\Http\Controllers\ConsultaController@generate_pdf');
    });
});