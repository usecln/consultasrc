<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Nacionalidad extends Model {
    protected $table = 'dbo.cat_nacionalidades';
    protected $primaryKey = 'CVE_NACIONALIDAD';
    public $timestamps = false;
    protected $fillable = [
        'CVE_NACIONALIDAD',
        'NACIONALIDAD'
    ];

    protected $hidden = [
        
    ];
    public function nacimientos()
    {
        return $this->hasMany('App\Nacimiento', 'CVE_NACIONALIDAD');
    }
}