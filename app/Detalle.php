<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {
    protected $table = 'dbo.detalles';
    public $timestamps = false;
    protected $fillable = [
        'CVE_OFICIALIA','ANO','TRAMITE','SERVICIO','CVE_DETALLE','CVE_NACIONALIDAD','NOMBRE','PRIMER_APELLIDO','SEGUNDO_APELLIDO',
        'EDAD','CVE_ESTADOCIVIL','CVE_ESTADO_DOMICILIO','CVE_MUNICIPIO_DOMICILIO','CVE_LOCALIDAD_DOMICILIO','CALLE_DOMICILIO',
        'NUMERO_DOMICILIO','CVE_COLONIA','COD_POSTAL_DOMICILIO','LUGAR_DOMICILIO','CVE_PARENTESCO','CVE_OCUPACION','LOGIN','CORREOE','TELNOM'
    ];

    protected $hidden = [
        'CVE_OFICIALIA','ANO','TRAMITE','SERVICIO','CVE_ESTADOCIVIL','CVE_ESTADO_DOMICILIO','CVE_MUNICIPIO_DOMICILIO','CVE_LOCALIDAD_DOMICILIO',
        'CALLE_DOMICILIO','NUMERO_DOMICILIO','CVE_COLONIA','COD_POSTAL_DOMICILIO','LUGAR_DOMICILIO','CVE_PARENTESCO','CVE_OCUPACION','LOGIN',
        'CORREOE','TELNOM', 'CVE_DETALLE', 'CVE_NACIONALIDAD'
    ];

    public function tipo_detalle()
    {
        return $this->belongsTo('App\TipoDetalle', 'CVE_DETALLE', 'CVE_DETALLE');
    }
    public function nacionalidad()
    {
        return $this->belongsTo('App\Nacionalidad', 'CVE_NACIONALIDAD', 'CVE_NACIONALIDAD');
    }
    public function scopeNoLock($query)
    {
        return $query->from(\DB::raw(self::getTable() . ' with (nolock)'));
    }
}