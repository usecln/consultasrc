<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Oficialia extends Model {
    protected $table = 'dbo.cat_oficialias';
    protected $primaryKey = 'CVE_OFICIALIA';
    public $timestamps = false;
    protected $fillable = [
        'CVE_OFICIALIA',
        'OFICIALIA',
        'CVE_LOCALIDAD',
        'CALLE',
        'NUMERO',
        'COD_POSTAL',
        'CVE_COLONIA',
        'TELEFONO',
        'CVE_ESTADO',
        'CVE_MUNICIPIO',
        'NUEVA',
        'CATEGORIA'
    ];

    protected $hidden = [
        'CVE_LOCALIDAD',
        'CALLE',
        'NUMERO',
        'COD_POSTAL',
        'CVE_COLONIA',
        'TELEFONO',
        'CVE_ESTADO',
        'CVE_MUNICIPIO',
        'NUEVA',
        'CATEGORIA'
    ];
    public function scopeNoLock($query)
    {
        return $query->from(\DB::raw(self::getTable() . ' with (nolock)'));
    }
}